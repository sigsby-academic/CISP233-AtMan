﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Prototype
{
	class Game
	{
		#region HELPERS
		// random
		Random rand = new Random();

		// distance
		public static double dist(int x1, int y1, int x2, int y2)
		{
			return Math.Sqrt(Math.Pow(Math.Abs(x1 - x2),2) + Math.Pow(Math.Abs(y1 - y2),2));
		}

		// helpers for calculating coordinate/position
		// does an actual modular arithmetic operator
		// used to wrap an objects position when it would go off the board
		public static int mod(int x, int m) { return (int)mod((double)x, (double)m); }
		public static double mod(double x, double m) { return (x % m + m) % m; }
		#endregion

		#region CONSTANTS
		// constants for game status
		// used for control within main loop
		public const int PAUSE = -1;
		public const int STOP = 0;
		public const int PLAY = 1;

		// scoring values
		public const int DOT_VALUE = 10;
		public const int POWER_VALUE = 50;
		public const int GHOST_VALUE = 200;
		public const int GHOST_VALUE_MULTIPLIER = 2;

		// ghost modes
		public const int MODE_SCATTER = 1;
		public const int MODE_CHASE = 2;
		public const int MODE_FRIGHTENED = 3;
		public const int MODE_FRIGHTENED_DURATION = 6000; // milliseconds
		#endregion

		#region PROFILING
		// time to wait before starting game (in milliseconds)
		// used at start of each level and each life
		private int readyWait = 3000;
		public int ReadyWait
		{
			get { return readyWait; }
			set { readyWait = value; }
		}

		// target framerate in frames/second
		// used to control the sleep value per frame
		private int targetFrameRate = 30;
		public int TargetFrameRate
		{
			get { return targetFrameRate; }
			set { targetFrameRate = value; }
		}

		// time to sleep per second (in milliseconds)
		// used to achieve target framerate
		private int sleep = 500;
		public int Sleep
		{
			get { return sleep; }
			set { sleep = value; }
		}
		public int SleepPerFrame
		{
			get { return sleep / targetFrameRate; }
		}

		// track overall time and frame
		// use to alter sleep time based on actual framerate
		// these should get reset between levels
		private Stopwatch timer = new Stopwatch();
		private long frame = 0;
		private TimeSpan frameStart;
		private TimeSpan frameStop;
		public double AverageFrameRate {
			get {
				double seconds = timer.Elapsed.Seconds;
				if(frame < 1 || seconds < 1) return 0.0;
				return frame / seconds;
			}
		}
		public double InstantFrameRate {
			get {
				return targetFrameRate * (frameStop - frameStart).Seconds;
			}
		}
		#endregion

		#region MAP/VIEW/DISPLAY
		// store the original map data
		private string[] map;
		public string Map
		{
			set {
				map = value.Split(
					new string[] { "\r\n", "\n" }, 
					StringSplitOptions.RemoveEmptyEntries
				);
				View = map;
			}
		}

		private Coordinate gate;

		// view, copy of map stored as 2-d array
		private char[,] view;
		private string[] View
		{
			set {
				view = new char[value[0].Length, value.Length];
				for (int y = 0; y < value.Length; y++)
				{
					for (int x = 0; x < value[y].Length; x++)
					{
						char c = value[y][x];
						if (Symbol.isFood(c))
						{
							food++;
						}
						else if (Symbol.isGate(c))
						{
							gate = new Coordinate(x, y);
						}
						else if (Symbol.isPlayer(c))
						{
							player.Spawn = new Coordinate(x, y);
							c = Symbol.EMPTY;
						}
						else if (Symbol.isBlinky(c))
						{
							blinky.Spawn = new Coordinate(x, y);
							c = Symbol.EMPTY;
						}
						else if (Symbol.isPinky(c))
						{
							pinky.Spawn = new Coordinate(x, y);
							c = Symbol.EMPTY;
						}
						else if (Symbol.isInky(c))
						{
							inky.Spawn = new Coordinate(x, y);
							c = Symbol.EMPTY;
						}
						else if (Symbol.isClyde(c))
						{
							clyde.Spawn= new Coordinate(x, y);
							c = Symbol.EMPTY;
						}
						view[x, y] = c;
					}
				}
				//if (player != null) display.View = view;
				//if (display != null ) display.View = view;
			}
		}
		private int ViewWidth { get { return view.GetLength(0); } }
		private int ViewHeight { get { return view.GetLength(1); } }

		// display, handles actual output of view to screen
		private Output.Interface display;
		public Output.Interface Display
		{
			get { return display; }
			set {
				display = value;
				//if(view != null) display.View = view;
				//if(player != null) display.Player = player;
			}
		}
		#endregion

		#region MOVEABLES
		// player and ghost objects
		private Player player;
		public Player Player {
			get { return player; }
			private set {
				player = value;
				//if(display != null) display.Player = player;
			}
		}

		private Ghost.Blinky blinky;
		public Ghost.Blinky Blinky
		{
			get { return blinky; }
			set { blinky = value; }
		}

		private Ghost.Pinky pinky;
		public Ghost.Pinky Pinky
		{
			get { return pinky; }
			set { pinky = value; }
		}

		private Ghost.Inky inky;
		public Ghost.Inky Inky
		{
			get { return inky; }
			set { inky = value; }
		}

		private Ghost.Clyde clyde;
		public Ghost.Clyde Clyde
		{
			get { return clyde; }
			set { clyde = value; }
		}
		#endregion

		// constructor
		public Game()
		{
			Player = new Player();
			Blinky = new Ghost.Blinky();
			Pinky = new Ghost.Pinky();
			Inky = new Ghost.Inky();
			Clyde = new Ghost.Clyde();
		}

		public void Start()
		{
			display.Player = player;
			display.Blinky = blinky;
			display.Pinky = pinky;
			display.Inky = inky;
			display.Clyde = clyde;
			display.View = view;
			display.Reset();
			play();
		}

		// game play
		private int status = PLAY;
		private int food = 0;
		private int mode = MODE_SCATTER;
		private int ghostValue = 0;
		private int ghostCounter = 0;
		private Stopwatch mtimer = new Stopwatch();
		private Stopwatch ftimer = new Stopwatch();

		private void startTimers()
		{
			timer.Start();
			if (mode == MODE_FRIGHTENED)
			{
				mtimer.Stop();
				ftimer.Start();
			}
			else
			{
				mtimer.Start();
				ftimer.Reset();
			}
		}
		private void restartTimers()
		{
			stopTimers();
			startTimers();
		}
		private void stopTimers()
		{
			timer.Stop();
			mtimer.Stop();
			ftimer.Stop();
		}

		private void play()
		{
			System.Threading.Thread.Sleep(ReadyWait);
			startTimers();
			while (status != STOP && food > 0)
			{
				frameStart = timer.Elapsed;
				frame++;
				System.Threading.Thread.Sleep(SleepPerFrame);

				setMode();
				handleInput();

				if (status == PLAY)
				{
					handleMovement();
					renderFrame();
					checkCollisions();
				}

				frameStop = timer.Elapsed;

				#if DEBUG
				stopTimers();
				display.DebugTimer = timer.Elapsed;
				display.DebugFrame = frame;
				display.DebugFrameRate = AverageFrameRate;
				
				display.DebugStatus = status;
				display.DebugFood = food;

				display.DebugMTimer = mtimer.Elapsed;
				display.DebugFTimer = ftimer.Elapsed;
				display.DebugMode = mode;
				
				display.DebugPlayer = player;
				display.DebugBlinky = blinky;
				startTimers();
				#endif
			}
			stopTimers();
			if (food > 0) display.YouLose();
			else display.YouWin();
			
		}

		private void pause()
		{
			status = PAUSE;
			stopTimers();
		}

		private void resume()
		{
			status = PLAY;
			restartTimers();
		}

		/* table of modes
		mode			level 1		level 2-4	level 5+
		--------------------------------------------------
		scatter		7				7				5
		chase			20				20				20
		scatter		7				7				5
		chase			20				20				20
		scatter		5				5				5
		chase			20				1033			1037
		scatter		5				1/60			1/60
		chase			~				~				~
		*/
		private void setMode()
		{
			if (mode != MODE_FRIGHTENED || ftimer.ElapsedMilliseconds > MODE_FRIGHTENED_DURATION)
			{
				double s = mtimer.ElapsedMilliseconds / 1000;
				if(s > 84) setModeChase();
				else if (s > 79) setModeScatter();
				else if (s > 59) setModeChase();
				else if (s > 54) setModeScatter();
				else if (s > 34) setModeChase();
				else if (s > 27) setModeScatter();
				else if (s > 7) setModeChase();
				else setModeScatter();
			}
		}
		private void setModeScatter()
		{
			if (mode == MODE_CHASE) reverseGhosts();
			mode = MODE_SCATTER;
			scatterGhosts();
			restartTimers();
		}
		private void setModeChase()
		{
			if (mode == MODE_SCATTER) reverseGhosts();
			chaseGhosts();
			mode = MODE_CHASE;
			restartTimers();
		}
		private void setModeFrightened()
		{
			mode = MODE_FRIGHTENED;
			frightenGhosts();
			ghostValue = GHOST_VALUE;
			restartTimers();
		}

		private void handleInput()
		{
			do
			{
				if (Console.KeyAvailable)
				{
					ConsoleKeyInfo input = Console.ReadKey(true);
					switch (input.Key)
					{
						case ConsoleKey.P:
							if (status == PAUSE) resume();
							else pause();
							break;
						case ConsoleKey.Q:
							status = STOP;
							break;
						case ConsoleKey.Spacebar:
							if (status == PLAY) player.Stop();
							break;
						case ConsoleKey.UpArrow:
							if (status == PLAY) player.FaceUp();
							break;
						case ConsoleKey.DownArrow:
							if (status == PLAY) player.FaceDown();
							break;
						case ConsoleKey.LeftArrow:
							if (status == PLAY) player.FaceLeft();
							break;
						case ConsoleKey.RightArrow:
							if (status == PLAY) player.FaceRight();
							break;
					}
				}
			}
			while (status == PAUSE);
		}

		private void handleMovement()
		{
			movePlayer();
			moveGhosts();
		}

		private void movePlayer()
		{
			double x = player.X;
			double y = player.Y;
			int? pending = player.PendingDirection;
			if (pending != null && pending != Moveable.STOP)
			{
				int dx = (int)x;
				int dy = (int)y;
				switch (pending)
				{
					case Moveable.UP: dy = mod(--dy, ViewHeight); break;
					case Moveable.DOWN: dy = mod(++dy, ViewHeight); break;
					case Moveable.LEFT: dx = mod(--dx, ViewWidth); break;
					case Moveable.RIGHT: dx = mod(++dx, ViewWidth); break;
				}
				if(!Symbol.isBlocking(view[dx,dy]))
				{
					player.Direction = (int)pending;
					player.PendingDirection = null;
				}
			}
			if (player.Direction != Moveable.STOP)
			{
				double dx = x;
				double dy = y;
				double speed = player.Speed / TargetFrameRate;
				switch (player.Direction)
				{
					case Moveable.UP: dy = mod(dy - speed, ViewHeight); break;
					case Moveable.DOWN: dy = mod(dy + speed, ViewHeight); break;
					case Moveable.LEFT: dx = mod(dx - speed, ViewWidth); break;
					case Moveable.RIGHT: dx = mod(dx + speed, ViewWidth); break;
				}
				if (!Symbol.isBlocking(view[(int)dx, (int)dy]))
				{
					//view[(int)x, (int)y] = map[(int)y][(int)x];
					//view[(int)dx, (int)dy] = Symbol.PLAYER;
					player.X = dx;
					player.Y = dy;
				}
			}
		}

		private void moveGhosts()
		{
			moveGhost(blinky);

			if(!pinky.Active) pinky.Release(gate);
			if(pinky.Active) moveGhost(pinky);

			if (!inky.Active && ghostCounter >= 30)
			{
				ghostCounter = 0;
				inky.Release(gate);
			}
			if(inky.Active) moveGhost(inky);

			if (!clyde.Active && ghostCounter >= 60)
			{
				ghostCounter = 0;
				clyde.Release(gate);
			}
			if (clyde.Active) moveGhost(clyde);
		}
		private void moveGhost(Ghost.Abstract ghost)
		{
			double x = ghost.X;
			double y = ghost.Y;
			double speed = ghost.Speed / TargetFrameRate;
			
			// get next cell with current direction
			double dx = x;
			double dy = y;
			switch(ghost.Direction)
			{
				case Moveable.UP: dy = mod(dy -= speed, ViewHeight); break;
				case Moveable.LEFT: dx = mod(dx -= speed, ViewWidth); break;
				case Moveable.DOWN: dy = mod(dy += speed, ViewHeight); break;
				case Moveable.RIGHT: dx = mod(dx += speed, ViewWidth); break;
			}
			// perform move
			if (ghostCanMove(view[(int)dx, (int)dy], ghost))
			{
				ghost.X = dx;
				ghost.Y = dy;
			}
			// recover if eaten
			if (ghost.Eaten && ghost.AtSpawn())
			{
				ghost.Reset();
			}
			// if changed grid cell
			if ((int)x != (int)ghost.X || (int)y != (int)ghost.Y)
			{
				// apply pending direction change from previous decision
				int? pending = ghost.PendingDirection;
				if (pending != null) ghost.Direction = (int)pending;
				// calculate next turn
				ghost.PendingDirection = (int?)decideGhostDirection(ghost);
			}
		}
		private int decideGhostDirection(Ghost.Abstract ghost)
		{
			// default to current direction
			int direction = ghost.Direction;

			// init distance, max for map, to track shortest
			double distance = dist(0, 0, ViewWidth, ViewHeight);

			// get target cell
			Coordinate target;
			if (ghost.Eaten) target = ghost.Spawn;
			else
			{
				switch (mode)
				{
					case MODE_FRIGHTENED:
						target = new Coordinate(
							rand.Next(ViewWidth), rand.Next(ViewHeight)
						);
						break;
					case MODE_SCATTER:
						target = ghost.GetScatterTarget();
						break;
					case MODE_CHASE: default:
						if (ghost.GetType() == typeof(Ghost.Inky))
						{
							target = ((Ghost.Inky)ghost).GetChaseTarget(player, blinky);
						}
						else target = ghost.GetChaseTarget(player);
						break;
				}
			}

			// get next cell coordinates
			int x = (int)ghost.X;
			int y = (int)ghost.Y;
			switch (ghost.Direction)
			{
				case Moveable.UP: y = mod(--y, ViewHeight); break;
				case Moveable.LEFT: x = mod(--x, ViewWidth); break;
				case Moveable.DOWN: y = mod(++y, ViewHeight); break;
				case Moveable.RIGHT: x = mod(++x, ViewWidth); break;
			}
			Coordinate up = new Coordinate(x, mod((y - 1), ViewHeight));
			Coordinate left = new Coordinate(mod((x - 1), ViewWidth), y);
			Coordinate down = new Coordinate(x, mod((y + 1), ViewHeight));
			Coordinate right = new Coordinate(mod((x + 1), ViewWidth), y);

			// up
			if(ghost.Direction != Moveable.DOWN)
			{
				if(ghostCanMove(view[up.X, up.Y], ghost))
				{
					double t = dist(up.X, up.Y, target.X, target.Y);
					if (t < distance)
					{
						direction = Moveable.UP;
						distance = t;
					}
				}
			}
			// left
			if(ghost.Direction != Moveable.RIGHT)
			{
				if (ghostCanMove(view[left.X, left.Y], ghost))
				{
					double t = dist(left.X, left.Y, target.X, target.Y);
					if (t < distance)
					{
						direction = Moveable.LEFT;
						distance = t;
					}
				}
			}
			// down
			if(ghost.Direction != Moveable.UP)
			{
				if (ghostCanMove(view[down.X, down.Y], ghost))
				{
					double t = dist(down.X, down.Y, target.X, target.Y);
					if (t < distance)
					{
						direction = Moveable.DOWN;
						distance = t;
					}
				}
			}
			// up
			if(ghost.Direction != Moveable.LEFT)
			{
				if (ghostCanMove(view[right.X, right.Y], ghost))
				{
					double t = dist(right.X, right.Y, target.X, target.Y);
					if (t < distance)
					{
						direction = Moveable.RIGHT;
						distance = t;
					}
				}
			}

			return direction;
		}
		private bool ghostCanMove(char c, Ghost.Abstract ghost)
		{
			return
				(ghost.Eaten && !Symbol.isWall(c) && !Symbol.isHouse(c))
				|| !Symbol.isBlocking(c)
			;
		}
		private void reverseGhosts()
		{
			if(blinky.Active) blinky.Reverse();
			if(pinky.Active) pinky.Reverse();
			if(inky.Active) inky.Reverse();
			if(clyde.Active) clyde.Reverse();
		}
		private void scatterGhosts()
		{
			blinky.Scatter = true;
			pinky.Scatter = true;
			inky.Scatter = true;
			clyde.Scatter = true;
		}
		private void chaseGhosts()
		{
			blinky.Chase = true;
			pinky.Chase = true;
			inky.Chase = true;
			clyde.Chase = true;
		}
		private void frightenGhosts()
		{
			blinky.Frightened = true;
			pinky.Frightened = true;
			inky.Frightened = true;
			clyde.Frightened = true;
		}

		private void checkCollisions()
		{
			int x = (int)player.X;
			int y = (int)player.Y;
			// food
			if (Symbol.isDot(view[x, y]))
			{
				view[x,y] = Symbol.EMPTY;
				player.Score += DOT_VALUE;
				food--;
				ghostCounter++;
			}
			else
			if (Symbol.isPower(view[x, y]))
			{
				view[x, y] = Symbol.EMPTY;
				player.Score += POWER_VALUE;
				food--;
				setModeFrightened();
			}
			// ghost collisions
			// check for death first
			if (ghostKillsPlayer()) handleDeath();
			// if we survived, check for ghosts to eat
			else playerEatsGhost();
		}
		private bool ghostKillsPlayer()
		{
			return
				ghostKillsPlayer(blinky)
			|| ghostKillsPlayer(pinky)
			|| ghostKillsPlayer(inky)
			|| ghostKillsPlayer(clyde)
			;
		}
		private bool ghostKillsPlayer(Ghost.Abstract ghost)
		{
			return
				(int)player.X == (int)ghost.X
			&& (int)player.Y == (int)ghost.Y
			&& !ghost.Eaten
			&& !ghost.Frightened
			;
		}

		private void playerEatsGhost()
		{
			playerEatsGhost(blinky);
			playerEatsGhost(pinky);
			playerEatsGhost(inky);
			playerEatsGhost(clyde);
		}
		private void playerEatsGhost(Ghost.Abstract ghost)
		{
			if(
				(int)ghost.X == (int)player.X
			&& (int)ghost.Y == (int)player.Y
			&& !ghost.Eaten
			&& ghost.Frightened
			) {
				ghost.Eaten = true;
				player.Score += ghostValue;
				ghostValue *= GHOST_VALUE_MULTIPLIER;
			}
		}

		private void handleDeath()
		{
			stopTimers();
			System.Threading.Thread.Sleep(ReadyWait);
			player.Die();
			if (player.Lives < 1)
			{
				status = STOP;
			}
			else
			{
				player.Reset();
				blinky.Reset();
				pinky.Reset();
				inky.Reset();
				clyde.Reset();
				display.Reset();
				System.Threading.Thread.Sleep(ReadyWait);
			}
			startTimers();
		}

		private void renderFrame()
		{
			display.Render();
		}

	}
}
