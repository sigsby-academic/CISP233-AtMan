﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Prototype
{
	abstract class Moveable
	{
		public const int LEFT = -2;
		public const int UP = -1;
		public const int STOP = 0;
		public const int DOWN = 1;
		public const int RIGHT = 2;

		private Coordinate spawn;
		public Coordinate Spawn
		{
			get { return spawn; }
			set { spawn = value; position = new Position((double)value.X, (double)value.Y); }
		}

		private double speed = 5.0;
		public double Speed
		{
			get { return speed; }
			set { if(value < 0) throw new Exception(); speed = value; }
		}

		private int direction = STOP;
		public int Direction
		{
			get { return direction; }
			set { if(value < LEFT || value > RIGHT) throw new Exception(); direction = value; }
		}

		private int? pendingDirection;
		public int? PendingDirection
		{
			get { return pendingDirection; }
			set {
				if(value < LEFT || value > RIGHT) throw new Exception();
				if(value == STOP) Direction = STOP;
				pendingDirection = value;
			}
		}
		
		private Position position;

		public double X
		{
			get { return position.X; }
			set { position.X = value; }
		}

		public double Y
		{
			get { return position.Y; }
			set { position.Y = value; }
		}

		public virtual void Reset()
		{
			Direction = STOP;
			position.X = spawn.X;
			position.Y = spawn.Y;
		}

	}
}
