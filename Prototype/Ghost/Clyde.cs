﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Prototype.Ghost
{
	class Clyde : Abstract
	{
		public Clyde() : base()
		{
			ScatterTarget = new Coordinate(0, 32);
		}
		public override Coordinate GetChaseTarget(Player p)
		{
			Coordinate t;
			double dist = Game.dist((int)p.X, (int)p.Y, (int)X, (int)Y);
			if(dist > 8) t = GetScatterTarget();
			else t = base.GetChaseTarget(p);
			return t;
		}
	}
}
