﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Prototype.Ghost
{
	class Blinky : Abstract
	{
		
		public Blinky() : base()
		{
			ScatterTarget = new Coordinate(24,-3);
			Active = true;
		}
		public override void Reset()
		{
			base.Reset();
			Active = true;
		}
	}
}
