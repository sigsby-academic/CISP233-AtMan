﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Prototype.Ghost
{
	abstract class Abstract : Moveable
	{

		private int mode;
		public int Mode
		{
			get { return mode; }
			set { mode = value; }
		}
		public bool Scatter
		{
			get { return mode == Game.MODE_SCATTER; }
			set { if(value == true) mode = Game.MODE_SCATTER; }
		}
		public bool Chase
		{
			get { return mode == Game.MODE_CHASE; }
			set { if (value == true) mode = Game.MODE_CHASE; }
		}
		public bool Frightened
		{
			get { return mode == Game.MODE_FRIGHTENED; }
			set { if (value == true) mode = Game.MODE_FRIGHTENED; }
		}

		private bool eaten = false;
		public bool Eaten
		{
			get { return eaten; }
			set { eaten = value; }
		}

		protected bool active = false;
		public bool Active
		{
			 get { return active; }
			 set { active  = value; }
		}

		private Coordinate scatterTarget;
		public Coordinate ScatterTarget
		{
			get { return scatterTarget; }
			set { scatterTarget = value; }
		}

		public Coordinate GetScatterTarget()
		{
			return ScatterTarget;
		}

		public virtual Coordinate GetChaseTarget(Player p)
		{
			return new Coordinate((int)p.X, (int)p.Y);
		}

		protected Abstract()
		{
			Direction = LEFT;
			PendingDirection = Direction;
		}

		public override void Reset()
		{
			base.Reset();
			Active = false;
			Scatter = true;
			Eaten = false;
			Direction = LEFT;
			PendingDirection = Direction;
		}

		public void Reverse()
		{
			if(Direction == UP) PendingDirection = DOWN;
			else if(Direction == LEFT) PendingDirection = RIGHT;
			else if(Direction == DOWN) PendingDirection = UP;
			else if(Direction == RIGHT) PendingDirection = LEFT;
		}

		public void Release(Coordinate gate)
		{
				Active = true;
				X = gate.X;
				Y = gate.Y;
				Direction = UP;
				PendingDirection = LEFT;
		}

		public bool AtSpawn()
		{
			return (int)X == Spawn.X && (int)Y == Spawn.Y;
		}

	}
}
