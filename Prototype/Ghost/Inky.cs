﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Prototype.Ghost
{
	class Inky : Abstract
	{
		public Inky() : base()
		{
			ScatterTarget = new Coordinate(27, 32);
		}
		public Coordinate GetChaseTarget(Player p, Blinky b)
		{
			int x = (int)p.X;
			int y = (int)p.Y;
			switch (p.Direction)
			{
				case UP: y += 2; break;
				case DOWN: y -= 2; break;
				case LEFT: x -= 2; break;
				case RIGHT: x += 2; break;
			}
			x = x + (x - (int)b.X);
			y = y + (y - (int)b.Y);
			return new Coordinate(x, y);
		}
	}
}
