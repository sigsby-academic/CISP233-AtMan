﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Prototype.Ghost
{
	class Pinky : Abstract
	{
		public Pinky() : base()
		{
			ScatterTarget = new Coordinate(3,-3);
		}
		public override Coordinate GetChaseTarget(Player p)
		{
			int x = (int)p.X;
			int y = (int)p.Y;
			switch(p.Direction)
			{
				case UP: y += 4; break;
				case DOWN: y -= 4; break;
				case LEFT: x -= 4; break;
				case RIGHT: x += 4; break;
			}
			return new Coordinate(x,y);
		}
	}
}
