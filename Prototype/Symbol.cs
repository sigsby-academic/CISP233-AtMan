﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Prototype
{
	static class Symbol
	{
		public const char EMPTY = '\0';
		public const char SPACE = ' ';

		#region BLOCKING
		#region WALL
		// ─│┌┐└┘├┤┬┴┼
		public const char WALL_V = '│';
		public const char WALL_H = '─';
		public const char WALL_NE = '┐';
		public const char WALL_NW = '┌';
		public const char WALL_SE = '┘';
		public const char WALL_SW = '└';

		private static char[] WALL = new char[] {
			WALL_V, WALL_H, WALL_NE, WALL_NW, WALL_SE, WALL_SW
		};
		public static bool isWall(char c) { return WALL.Contains(c); }
		#endregion

		#region HOUSE
		// ═║╔╗╚╝╠╣╦╩╬
		public const char HOUSE_V = '║';
		public const char HOUSE_H = '═';
		public const char HOUSE_NE = '╗';
		public const char HOUSE_NW = '╔';
		public const char HOUSE_SE = '╝';
		public const char HOUSE_SW = '╚';

		private static char[]HOUSE = new char[] {
			HOUSE_V, HOUSE_H, HOUSE_NE, HOUSE_NW, HOUSE_SE, HOUSE_SW
		};
		public static bool isHouse(char c) { return HOUSE.Contains(c); }
		#endregion

		#region GATE
		// v^><
		public const char GATE_NS = 'v';
		public const char GATE_SN = '^';
		public const char GATE_EW = '<';
		public const char GATE_WE = '>';

		private static char[] GATE = new char[] {
			GATE_NS, GATE_SN, GATE_EW, GATE_WE
		};
		public static bool isGate(char c) { return GATE.Contains(c); }
		#endregion

		public static bool isBlocking(char c)
		{
			return isWall(c) || isHouse(c) || isGate(c);
		}
		#endregion

		#region FOOD
		// ♦ ∙ O ☼
		public const char DOT = '∙';
		public static bool isDot(char c) { return DOT == c; }

		public const char POWER = '♦';
		public static bool isPower(char c) { return POWER == c; }

		public static char[] FOOD = new char[] { DOT, POWER };
		public static bool isFood(char c) { return FOOD.Contains(c); }
		#endregion

		// MOVEABLE / CHARACTERS
		// @ a b c d
		// 001 ☺
		// 002 ☻
		// 173 ¡
		// 239 ∩
		// ©¨
		public const char PLAYER = '@';
		public static bool isPlayer(char c) { return PLAYER == c; }

		public const char BLINKY = 'b';
		public static bool isBlinky(char c) { return BLINKY == c; }

		public const char PINKY = 'p';
		public static bool isPinky(char c) { return PINKY == c; }

		public const char INKY = 'i';
		public static bool isInky(char c) { return INKY == c; }

		public const char CLYDE = 'c';
		public static bool isClyde(char c) { return CLYDE == c; }

		private static char[] GHOST = new char[] {
			BLINKY, PINKY, INKY, CLYDE
		};
		public static bool isGhost(char c) { return GHOST.Contains(c); }

		public const char GHOST_NORMAL = '☻';
		public const char GHOST_FRIGHTENED = '☺';
		public const char GHOST_EATEN = '¨';
	}
}
