﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Prototype.Output
{
	abstract class WinCon : Interface
	{
		#region RESTORE
		private int RESTORE_WINDOW_WIDTH = Console.WindowWidth;
		private int RESTORE_WINDOW_HEIGHT = Console.WindowHeight;
		private int RESTORE_BUFFER_WIDTH = Console.BufferWidth;
		private int RESTORE_BUFFER_HEIGHT = Console.BufferHeight;

		private void restore()
		{
			clear();
			Console.SetBufferSize(RESTORE_BUFFER_WIDTH, RESTORE_BUFFER_HEIGHT);
			Console.SetWindowSize(RESTORE_WINDOW_WIDTH, RESTORE_WINDOW_HEIGHT);
		}
		#endregion

		#region PADDING
		protected int PAD_TOP = 3;
		protected int PAD_BOTTOM = 2;
		protected int PAD_LEFT = 0;
		#if DEBUG
		protected int PAD_RIGHT = 50;
		protected string DEBUG_FORMAT = "  {0,-15}: {1,-30}";
		protected ConsoleColor DEBUG_COLOR = ConsoleColor.White; 
		#else
		protected int PAD_RIGHT = 0;
		#endif
		#endregion

		#region COLOR
		public const ConsoleColor DEFAULT_COLOR = ConsoleColor.White;

		public const ConsoleColor GATE_COLOR = ConsoleColor.Gray;
		public const ConsoleColor BLOCKING_COLOR = ConsoleColor.Blue;
		public const ConsoleColor FOOD_COLOR = ConsoleColor.DarkYellow;
		public const ConsoleColor PLAYER_COLOR = ConsoleColor.Yellow;
		public const ConsoleColor BLINKY_COLOR = ConsoleColor.Red;
		public const ConsoleColor PINKY_COLOR = ConsoleColor.Magenta;
		public const ConsoleColor INKY_COLOR = ConsoleColor.Cyan;
		public const ConsoleColor CLYDE_COLOR = ConsoleColor.DarkMagenta;
		public const ConsoleColor FRIGHTENED_COLOR = ConsoleColor.Blue;
		public const ConsoleColor EATEN_COLOR = ConsoleColor.Gray;

		public const ConsoleColor HEADER_COLOR = DEFAULT_COLOR;
		public const ConsoleColor SCORE_COLOR = DEFAULT_COLOR;
		public const ConsoleColor LIVES_COLOR = PLAYER_COLOR;
		public const ConsoleColor READY_COLOR = PLAYER_COLOR;
		public const ConsoleColor YOULOSE_COLOR = DEFAULT_COLOR;
		public const ConsoleColor YOUWIN_COLOR = DEFAULT_COLOR;
		#endregion

		#region STRINGS
		public const string HEADER_FORMAT = "{0,6}   HIGH SCORE   {1,-6}";
		public const string HEADER_1UP = "1UP";
		public const string HEADER_2UP = "2UP";
		public const string SCORE_FORMAT = "{0,7}";
		public const string LIVES_FORMAT = "  {0,-12}";
		public const string READY_STRING = "READY!";
		//public const string GAMEOVER_STRING = "GAMEOVER";
		public const string YOULOSE_STRING = "YOU LOSE";
		public const string YOUWIN_STRING = "YOU WIN!";
		#endregion

		#region POSITION
		public const int READY_ROW = 17;
		public const int YOULOSE_ROW = READY_ROW;
		public const int YOUWIN_ROW = YOULOSE_ROW;
		#endregion


		protected char[,] view;
		public virtual char[,] View {
			get { return view; }
			set { view = value; resize(); }
		}

		protected int ViewWidth { get { return view.GetLength(0); } }
		protected int ViewHeight { get { return view.GetLength(1); } }

		protected int Width { get { return Console.BufferWidth; } }
		protected int Height { get { return Console.BufferHeight; } }

		protected Player player;
		public Player Player {
			get { return player; }
			set { player = value; }
		}

		protected Ghost.Blinky blinky;
		public Ghost.Blinky Blinky
		{
			get { return blinky; }
			set { blinky = value; }
		}

		protected Ghost.Pinky pinky;
		public Ghost.Pinky Pinky
		{
			get { return pinky; }
			set { pinky = value; }
		}

		protected Ghost.Inky inky;
		public Ghost.Inky Inky
		{
			get { return inky; }
			set { inky = value; }
		}

		protected Ghost.Clyde clyde;
		public Ghost.Clyde Clyde
		{
			get { return clyde; }
			set { clyde = value; }
		}

		public WinCon()
		{
			init();
		}
		public WinCon(string title) {
			init();
			Console.Title = title;
		}
		~WinCon() { restore(); }

		private void init()
		{
			Console.CursorVisible = false;
			clear();
		}

		private void clear()
		{
			Console.Clear();
		}
		
		private void resize()
		{
			clear();
			int width = ViewWidth + PAD_LEFT + PAD_RIGHT;
			int height = ViewHeight + PAD_TOP + PAD_BOTTOM;
			Console.SetWindowSize(width, height);
			Console.SetBufferSize(width, height);
		}

		// interface implementation
		public void Reset()
		{
			renderHeader();
			renderFooter();
			Render();
			renderReady();
		}

		// interface implementation
		public void Render()
		{
			renderScore();
			renderView();
		}

		public void YouLose()
		{
			renderYouLose();
		}

		public void YouWin()
		{
			renderYouWin();
		}

		private void renderHeader()
		{
			Console.SetCursorPosition(0, 0);
			Console.ForegroundColor = HEADER_COLOR;
			Console.Write(HEADER_FORMAT, HEADER_1UP, "");
		}

		private void renderFooter()
		{
			string lives = "";
			for(int i = 1; i < player.Lives; i++) lives += Symbol.PLAYER;
			Console.SetCursorPosition(0, (Height-2));
			Console.ForegroundColor = LIVES_COLOR;
			Console.Write(LIVES_FORMAT, lives);
		}

		private void renderScore()
		{
			Console.SetCursorPosition(0, 1);
			Console.ForegroundColor = SCORE_COLOR;
			Console.Write(SCORE_FORMAT, player.Score);
		}

		protected virtual void renderReady()
		{
			int x = (int)(ViewWidth / 2) - (int)(READY_STRING.Length / 2) + PAD_LEFT;
			int y = READY_ROW + PAD_TOP;
			Console.SetCursorPosition(x,y);
			Console.ForegroundColor = READY_COLOR;
			Console.Write(READY_STRING);
		}

		protected virtual void renderYouLose()
		{
			int x = (int)(ViewWidth / 2) - (int)(YOULOSE_STRING.Length / 2) + PAD_LEFT;
			int y = YOULOSE_ROW + PAD_TOP;
			Console.SetCursorPosition(x, y);
			Console.ForegroundColor = YOULOSE_COLOR;
			Console.Write(YOULOSE_STRING);
		}

		protected virtual void renderYouWin()
		{
			int x = (int)(ViewWidth / 2) - (int)(YOUWIN_STRING.Length / 2) + PAD_LEFT;
			int y = YOUWIN_ROW + PAD_TOP;
			Console.SetCursorPosition(x, y);
			Console.ForegroundColor = YOUWIN_COLOR;
			Console.Write(YOUWIN_STRING);
		}

		protected abstract void renderView();

		#region DEBUG
		#if DEBUG
		public void DebugPrint(int y, string label, string value)
		{
			Console.SetCursorPosition(Width - PAD_RIGHT, y);
			Console.ForegroundColor = DEBUG_COLOR;
			Console.Write(DEBUG_FORMAT, label, value);
		}
		public int DebugPrint(int y, string label, Moveable value)
		{
			DebugPrint(y++, label, "");
			DebugPrint(y++, "SPAWN", value.Spawn.X + "," + value.Spawn.Y);
			DebugPrint(y++, "POSITION", Math.Round(value.X,5) + "," + Math.Round(value.Y,5));
			DebugPrint(y++, "SPEED", value.Speed.ToString());
			DebugPrint(y++, "DIRECTION", value.Direction.ToString());
			return y;
		}

		public TimeSpan DebugTimer { set { DebugPrint(1, "TIMER", value.ToString()); } }
		public long DebugFrame { set { DebugPrint(2, "FRAME", value.ToString()); } }
		public double DebugFrameRate { set { DebugPrint(3, "FRAMERATE", value.ToString()); } }

		public int DebugStatus { set { DebugPrint(5, "STATUS", value.ToString()); } }
		public int DebugFood { set { DebugPrint(6, "FOOD", value.ToString()); } }

		public TimeSpan DebugMTimer { set { DebugPrint(8, "MTIMER", value.ToString()); } }
		public TimeSpan DebugFTimer { set { DebugPrint(9, "FTIMER", value.ToString()); } }
		public int DebugMode { set { DebugPrint(10, "MODE", value.ToString()); } }

		public Player DebugPlayer { set {
			int y = DebugPrint(12, "PLAYER", value);
			DebugPrint(y++, "PENDING", value.PendingDirection.ToString());
		} }

		public Ghost.Blinky DebugBlinky
		{
			set
			{
				int y = DebugPrint(19, "Blinky", value);
				DebugPrint(y++, "PENDING", value.PendingDirection.ToString());
			}
		}

		#endif
		#endregion

	}
}
