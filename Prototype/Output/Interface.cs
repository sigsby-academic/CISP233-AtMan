﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Prototype.Output
{
	interface Interface
	{
		char[,] View { get; set; }
		Player Player { get; set; }
		Ghost.Blinky Blinky { get; set; }
		Ghost.Pinky Pinky { get; set; }
		Ghost.Inky Inky { get; set; }
		Ghost.Clyde Clyde { get; set; }

		void Reset();
		void Render();
		void YouLose();
		void YouWin();

		#if DEBUG
		void DebugPrint(int y, string label, string value);
		int DebugPrint(int y, string label, Moveable value);

		TimeSpan DebugTimer { set; }
		long DebugFrame { set; }
		double DebugFrameRate { set; }

		int DebugStatus { set; }
		int DebugFood { set; }

		TimeSpan DebugMTimer { set; }
		TimeSpan DebugFTimer { set; }
		int DebugMode { set; }

		Player DebugPlayer { set; }
		Ghost.Blinky DebugBlinky { set; }
		#endif
	}
}
