﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Prototype.Output.Default
{
	class Display : WinCon
	{
		// stores the last view rendered
		// Consol.Write is expensive
		// this allows us to write changes
		private char[,] buffer;

		// override reset buffer when view is set
		public override char[,] View
		{
			get { return base.View; }
			set {
				base.View = value;
				buffer = new char[ViewWidth,ViewHeight];
			}
		}

		protected override void renderView()
		{
			for (int x = 0; x < ViewWidth; x++)
			{
				for (int y = 0; y < ViewHeight; y++)
				{
					char c;
					if (x == (int)blinky.X && y == (int)blinky.Y) c = Symbol.BLINKY;
					else if (x == (int)pinky.X && y == (int)pinky.Y) c = Symbol.PINKY;
					else if (x == (int)inky.X && y == (int)inky.Y) c = Symbol.INKY;
					else if (x == (int)clyde.X && y == (int)clyde.Y) c = Symbol.CLYDE;
					else if (x == (int)player.X && y == (int)player.Y) c = Symbol.PLAYER;
					else c = view[x, y];
					if (buffer[x, y] != c)
					{
						buffer[x, y] = c;
						ConsoleColor color = DEFAULT_COLOR;
						if (Symbol.isGate(c))
						{
							color = GATE_COLOR;
							switch (c)
							{
								case Symbol.GATE_NS:
								case Symbol.GATE_SN: c = Symbol.WALL_H; break;
								case Symbol.GATE_EW:
								case Symbol.GATE_WE: c = Symbol.WALL_V; break;
							}
						}
						else if (Symbol.isBlocking(c))
						{
							color = BLOCKING_COLOR;
						}
						else if (Symbol.isFood(c))
						{
							color = FOOD_COLOR;
						}
						else if (Symbol.isPlayer(c))
						{
							color = PLAYER_COLOR;
						}
						else if (Symbol.isBlinky(c))
						{
							if(blinky.Eaten)
							{
								c = Symbol.GHOST_EATEN;
								color = EATEN_COLOR;
							}
							else if(blinky.Frightened)
							{
								c = Symbol.GHOST_FRIGHTENED;
								color = FRIGHTENED_COLOR;
							}
							else
							{
								c = Symbol.GHOST_NORMAL;
								color = BLINKY_COLOR;
							}
						}
						else if (Symbol.isPinky(c))
						{
							if (pinky.Eaten)
							{
								c = Symbol.GHOST_EATEN;
								color = EATEN_COLOR;
							}
							else if (pinky.Frightened)
							{
								c = Symbol.GHOST_FRIGHTENED;
								color = FRIGHTENED_COLOR;
							}
							else
							{
								color = PINKY_COLOR;
								c = Symbol.GHOST_NORMAL;
							}
						}
						else if (Symbol.isInky(c))
						{
							if (inky.Eaten)
							{
								c = Symbol.GHOST_EATEN;
								color = EATEN_COLOR;
							}
							else if (inky.Frightened)
							{
								c = Symbol.GHOST_FRIGHTENED;
								color = FRIGHTENED_COLOR;
							}
							else
							{
								color = INKY_COLOR;
								c = Symbol.GHOST_NORMAL;
							}
						}
						else if (Symbol.isClyde(c))
						{
							if (clyde.Eaten)
							{
								c = Symbol.GHOST_EATEN;
								color = EATEN_COLOR;
							}
							else if (clyde.Frightened)
							{
								c = Symbol.GHOST_FRIGHTENED;
								color = FRIGHTENED_COLOR;
							}
							else
							{
								color = CLYDE_COLOR;
								c = Symbol.GHOST_NORMAL;
							}
						}
						render(x, y, color, c);
					}
				}
			}
		}

		private void render(int x, int y, ConsoleColor color, char c)
		{
			Console.SetCursorPosition((x + PAD_LEFT), (y + PAD_TOP));
			Console.ForegroundColor = color;
			Console.Write(c);
		}

		// since this writes out to the view area
		// it won't be overwritten with the correct view contents
		// by adding to the buffer, the view won't match next render and
		// the ready string will be overwritten with the correct view content
		protected override void renderReady()
		{
			base.renderReady();
			int w = READY_STRING.Length;
			int x = (int)(ViewWidth / 2) - (int)(w / 2);
			int y = READY_ROW;
			for(int i = 0; i < w; i++) buffer[(x+i),y] = READY_STRING[i];
		}

	}
}
