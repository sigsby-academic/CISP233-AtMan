﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Prototype
{
	class Program
	{
		public const string TITLE = "AtMan";

		// character speeds in cells/second
		public const int PLAYER_SPEED = 3;
		public const int GHOST_SPEED = 3;
		public const int BLINKY_SPEED = GHOST_SPEED;
		public const int PINKY_SPEED = GHOST_SPEED;
		public const int INKY_SPEED = GHOST_SPEED;
		public const int CLYDE_SPEED = GHOST_SPEED;

		private static Game game;

		[STAThread]
		static void Main(string[] args)
		{
			game = new Game();
			game.Map = Properties.Resources.MapDefault;
			game.Display = new Output.Default.Display();
			game.Player.Speed = PLAYER_SPEED;
			game.Blinky.Speed = BLINKY_SPEED;
			game.Pinky.Speed = PINKY_SPEED;
			game.Inky.Speed = INKY_SPEED;
			game.Clyde.Speed = CLYDE_SPEED;
			game.Start();

			Console.ReadKey(true);
		}
	}
}
