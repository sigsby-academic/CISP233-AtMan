﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Prototype
{
	class Coordinate
	{
		
		private int x;
		public int X {
			get { return x; }
			private set { x = value; }
		}

		private int y;
		public int Y {
			get { return y; }
			private set { y = value; }
		}

		public Coordinate(int x, int y)
		{
			X = x;
			Y = y;
		}

	}
}
