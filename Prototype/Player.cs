﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Prototype
{
	class Player : Moveable
	{
		private int lives = 3;
		public int Lives
		{
			get { return lives; }
			set { lives = value; }
		}

		private int score = 0;
		public int Score
		{
			get { return score; }
			set { score = value; }
		}

		public void FaceLeft() { PendingDirection = LEFT; }
		public void FaceUp() { PendingDirection = UP; }
		public void Stop() { PendingDirection = STOP; }
		public void FaceDown() { PendingDirection = DOWN; }
		public void FaceRight() { PendingDirection = RIGHT; }

		public void Die()
		{
			lives--;
		}

	}
}
