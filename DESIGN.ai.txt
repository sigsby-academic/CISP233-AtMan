
Ghosts have 3 modes: chase, scatter, frightened

chase is base on player coordinate
scatter is a fixed tile

switch between scatter mode is base on a timer
timer is reset between levels and on a new life
timer is paused while ghosts are in frightened mode

frightened mode happens when a power (energizer) is eaten
in frightened mode direction is chosen randomly

start in scatter

level 1
scatter for 7 seconds, chase for 20 seconds
scatter for 7 seconds, chase for 20 seconds
scatter for 5 seconds, chase for 20 seconds
scatter for 5 seconds, chase permanently for level

never reverse
	except on change from scatter or chase to any other mode
	then force reverse when entering next tile

on enter a new tile
	look ahead to the next tile
	decide direction for next tile

forced direction reverse applies to ghosts in house
they will reverse direction as soon as they leave the house




Mode			Level 1		Level 2-4	Level 5+
--------------------------------------------------
scatter		7				7				5
chase			20				20				20
scatter		7				7				5
chase			20				20				20
scatter		5				5				5
chase			20				1033			1037
scatter		5				1/60			1/60
chase			~				~				~
